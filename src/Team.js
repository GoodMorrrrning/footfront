class Team {
    constructor(id, name, shortName, tla, crest, legacy_id) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.tla = tla;
        this.crest = crest;
        this.legacy_id = legacy_id
    }
}
class FootballMatch {
    constructor(id, utcDate, status, matchday, stage, group, lastUpdated, homeTeam, awayTeam, score) {
      this.id = id;
      this.utcDate = utcDate;
      this.status = status;
      this.matchday = matchday;
      this.stage = stage;
      this.group = group;
      this.lastUpdated = lastUpdated;
      this.homeTeam = homeTeam;
      this.awayTeam = awayTeam;
      this.score = score;
    }}

    class Predictresult {
        constructor(home_result, away_result) {
            this.home_result = home_result;
            this.away_result = away_result;
        }
    }

export default class TeamApiHandler {
    constructor(apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    async getTeamsByName(teamName) {
        const apiUrl = `${this.apiBaseUrl}/team/teamByName?teamname=${teamName}`;
    
        try {
            const response = await fetch(apiUrl);
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
    
            const data = await response.json();
    
            // Utiliser un ensemble (Set) pour supprimer les doublons basés sur le champ name
            const uniqueTeams = new Set(data.map(teamData => teamData.name));
    
            // Créer un tableau unique en utilisant l'ensemble
            const uniqueTeamsArray = [...uniqueTeams];
    
            // Créer un tableau d'objets Team à partir des noms uniques
            const teams = uniqueTeamsArray.map(teamName => {
                const teamData = data.find(teamData => teamData.name === teamName);
                return new Team(
                    teamData.id,
                    teamData.name,
                    teamData.shortName,
                    teamData.tla,
                    teamData.crest,
                    teamData.legacyId
                );
            });
    
            return teams;
        } catch (error) {
            console.error('Error fetching data:', error);
            return [];
        }
    }

    async getFaceToFaceByMatchId(teamid) {
        const endpointUrl = `${this.apiBaseUrl}/match/getNextMatchForTeam?teamid=${teamid}`;
        try {
            const response = await fetch(endpointUrl); // Fix typo: apiUrl -> endpointUrl
            if (!response.ok) {
                return null
            }
            const data = await response.json();
            const footballMatch = new FootballMatch(
                data.id,
                data.utcDate,
                data.status,
                data.matchday,
                data.stage,
                data.group,
                data.lastUpdated,
                data.homeTeam,
                data.awayTeam,
                data.score
            );
            return await this.getPredictionResult(footballMatch);
        } catch (error) {
            console.error('Error fetching data:', error);
            return null; // Retourner null en cas d'erreur
        }
    }

    async getPredictionResult(footballMatch){
        let away = footballMatch.awayTeam.name
        let home = footballMatch.homeTeam.name
        let date = footballMatch.utcDate
        let apiEndpointurl = `http://martin-genereux.fr:1919?home=${home}&away=${away}&date=${date}`
        apiEndpointurl = URLify(apiEndpointurl);
        console.log(apiEndpointurl);
        try{
            const resp = await fetch(apiEndpointurl);
            console.log(resp);
            if (!resp.ok) {
                return null
            }
            const data = await resp.json();
            
            footballMatch.homeTeam.predictedresult = data.score_home;
            footballMatch.awayTeam.predictedresult = data.score_away;
            return footballMatch;
        }
        catch(e){
            console.log(e);
            return null
        }
    }
    
    
}
function URLify(string) {
    return string.trim().replace(/\s/g, '%20');
  }



//martin-genereux.fr:8080/match/getNextMatchForTeam?teamid=86
//reponse : 

/*
{
    "id": 438772,
    "utcDate": "2024-03-31T19:00:00Z",
    "status": "TIMED",
    "matchday": 30,
    "stage": "REGULAR_SEASON",
    "group": null,
    "lastUpdated": "2024-03-22T15:21:34Z",
    "homeTeam": {
        "id": 86,
        "name": "Real Madrid CF",
        "shortName": "Real Madrid",
        "tla": "RMA",
        "crest": "https://crests.football-data.org/86.png"
    },
    "awayTeam": {
        "id": 77,
        "name": "Athletic Club",
        "shortName": "Athletic",
        "tla": "ATH",
        "crest": "https://crests.football-data.org/77.png"
    },
    "score": {
        "winner": null,
        "duration": "REGULAR",
        "fullTime": {
            "home": 0,
            "away": 0
        },
*/
/* a partir de l'id de l'equipe en fonction de la recherche lancer cette requete et interoger FLASK sur les deux equipes */