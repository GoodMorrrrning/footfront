#!/usr/bin/env bash
CONTAINER_ID=$(docker ps -aqf "name=footvuejs")
echo $CONTAINER_ID
if [ ! -z "$CONTAINER_ID" ]; then
    docker stop $CONTAINER_ID
    docker rm -f $CONTAINER_ID
    docker wait $CONTAINER_ID
fi

for id in $(docker ps -q)
do
    if [[ $(docker port "${id}" | awk -F':' '{print $1}') == "${1}" ]]; then
        echo "stopping container ${id}"
        docker stop "${id}"
        docker rm -f "${id}"
        docker wait "${id}"
    fi
done

